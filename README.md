# 🚀 Projeto Automation Exercise

Este é um projeto Maven para automação de testes utilizando **Selenium**, **Cucumber** e **JUnit**.

## 🛠️ Requisitos

- ☕ **Java 11** ou superior
- 🐍 **Maven 3.6.0** ou superior

## 🧰 Tecnologias Utilizadas

- 🖱️ **Selenium**
- 🥒 **Cucumber**
- ✅ **JUnit**
- 🤹 **JavaFaker**
- 🪟 **Fest-Swing**

**📦 Instalação**

Siga os passos abaixo para configurar o ambiente e instalar as dependências do projeto:

**Clone o repositório:**

git clone https://github.com/seu-usuario/projeto_automation_exercise.git

**Navegue até o diretório do projeto:**

sh

**Copiar código**

cd projeto_automation_exercise

**Instalar dependências**

mvn install


📝 **Uso**

Executar os testes a partir da classe Runner dentro da pasta runner do projeto, colocando a tag da feature e do caso de teste.


🤝 **Contribuição**

Contribuições são bem-vindas! Siga os passos abaixo para contribuir:

**Faça um fork do projeto.**

* Crie uma branch para a sua feature (git checkout -b feature/sua-feature).

* Commit suas mudanças (git commit -m 'Adiciona a nova feature').

* Faça um push para a branch (git push origin feature/sua-feature).

* Abra um Pull Request.


🐛 **Reportando Problemas**

Se encontrar algum problema, por favor abra uma issue no GitLab detalhando o problema encontrado e, se possível, os passos para reproduzi-lo.


## 📁 Estrutura do Projeto

```plaintext
projeto_automation_exercise
│
├── src
│   ├── main
│   │   └── java
│   │
│   └── test
│       └── java
│
├── pom.xml
│
└── README.md