@Cart
  Feature: Tela de Carrinho de Compras

    Background: Realizar Testes no Carrinho de Compras
      Given acessar tela de cadastro
      When preencher os campos iniciais para cadastro
      And clicar em Signup
      When preencher o resto dos campos para cadastro
      And clicar em Create Account
      And clicar em Continue

      @CT001
      Scenario: Realizar Inclusão de Item no Carrinho
        When clicar em Cart
        And clicar no link Here
        And adiciona o produto no carrinho
        Then valido que item foi adicionado ao carrinho

      @CT002
      Scenario: Realizar Inclusão de Mais de um Item no Carrinho
        When clicar em Cart
        And clicar no link Here
        And adiciona o produto no carrinho
        And clicar no botao Continue Shopping
        And adiciona o produto no carrinho
        And clicar no link View Cart
        Then valido que item foi adicionado duas vezes

      @CT003
      Scenario: Realizar Exclusão de Item do Carrinho
        When clicar em Cart
        And clicar no link Here
        And adiciona o produto no carrinho
        And clicar no link View Cart
        And clicar no icone de exclusao
        Then valido que item foi excluido do carrinho

      @CT004
      Scenario: Realizar Check-out no Produto
        When clicar em Cart
        And clicar no link Here
        And adiciona o produto no carrinho
        And clicar no link View Cart
        And clicar no botao Proceed To Checkout
        When escrever comentario sobre processo de entrega
        And clicar no botao Place Order
        When preenche os campos com os dados do cartao de credito
        And clicar no botao Pay and Confirm Order
        Then valido que item foi pago com sucesso