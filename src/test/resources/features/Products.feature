@Products
  Feature: Tela de Produtos

    Background: Realizar Testes na Tela de Produtos
      Given acessar tela de cadastro
      When preencher os campos iniciais para cadastro
      And clicar em Signup
      When preencher o resto dos campos para cadastro
      And clicar em Create Account
      And clicar em Continue
      Given acessar tela Produtos

      @CT001
      Scenario: Realizar Inclusão de Item Estando Logado na Conta
        When adiciona o produto no carrinho
        And clicar no link View Cart
        Then valido que item foi adicionado ao carrinho com icone de exclusao

      @CT002
      Scenario: Realizar Inclusão de Item e Continuar Comprando
        When adiciona o produto no carrinho
        And clicar no botao Continue Shopping
        Then valido a permanencia na tela Home

      @CT003
      Scenario: Visualizar Especificação do Item
        When clicar no link View Product
        Then valido que a tela de detalhes do produto foi apresentada

      @CT004
      Scenario: Relizar Pesquisa de Produto
        When preencher o campo Search Product
          | search     |
          | Men Tshirt |
        And clicar no botao Search Product
        Then valido que o produto pesquisado foi apresentado

      @CT005
      Scenario: Realizar Busca de Item pelo Filtro de Categoria - Women
        When seleciono a categoria women
        And seleciono o filtro dress
        Then valido a tela do filtro Women - Dress Products
        When seleciono a categoria women
        And seleciono o filtro tops
        Then valido a tela do filtro Women - Tops Products
        When seleciono a categoria women
        And seleciono o filtro saree
        Then valido a tela do filtro Women - Saree Products

      @CT006
      Scenario: Realizar Busca de Item pelo Filtro de Categoria - Men
        When seleciono a categoria men
        And seleciono o filtro tshirt
        Then valido a tela do filtro Men - Tshirts Products
        When seleciono a categoria men
        And seleciono o filtro jeans
        Then valido a tela do filtro Men - Jeans Products

      @CT007
      Scenario: Realizar Busca de Item pelo Filtro de Categoria - Kids
        When seleciono a categoria kids
        And seleciono o filtro kids dress
        Then valido a tela do filtro Kids - Dress Products
        When seleciono a categoria kids
        And seleciono o filtro tops shirts
        Then valido a tela do filtro Kids - Tops & Shirts Products

      @CT008
      Scenario: Realizar Busca de Item pelo Filtro de Marca - Polo
        When seleciono a marca polo
        Then valido a tela da marca Brand - Polo Products
        When seleciono a marca h&m
        Then valido a tela da marca Brand - H&M Products
        When seleciono a marca madame
        Then valido a tela da marca Brand - Madame Products
        When seleciono a marca mast&harbour
        Then valido a tela da marca Brand - Mast & Harbour Products
        When seleciono a marca babyhug
        Then valido a tela da marca Brand - Babyhug Products
        When seleciono a marca allen solly junior
        Then valido a tela da marca Brand - Allen Solly Junior Products
        When seleciono a marca kookie kids
        Then valido a tela da marca Brand - Kookie Kids Products
        When seleciono a marca biba
        Then valido a tela da marca Brand - Biba Products