@Home
  Feature: Tela Inicial

    Background: Realizar Testes na Tela Inicial

      @CT001
      Scenario: Realizar Inclusão de Item Estando Logado na Conta
        Given acessar tela de cadastro
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar em Continue
        And adiciona o produto no carrinho
        And clicar no link View Cart
        Then valido que item foi adicionado ao carrinho com icone de exclusao

      @CT002
      Scenario: Realizar Inclusão de Item sem Estar Logado na Conta
        Given acessar tela Home
        And adiciona o produto no carrinho
        And clicar no link View Cart
        Then valido que item foi adicionado ao carrinho com icone de exclusao

      @CT003
      Scenario: Realizar Inclusão de Item e Continuar Comprando
        Given acessar tela Home
        And adiciona o produto no carrinho
        And clicar no botao Continue Shopping
        Then valido a permanencia na tela Home

      @CT004
      Scenario: Visualizar Especificação do Item
        Given acessar tela de cadastro
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar em Continue
        Given acessar tela Home
        And clicar no link View Product
        Then valido que a tela de detalhes do produto foi apresentada

      @CT005
      Scenario: Validar Carousel de Anúncios
        Given acessar tela Home
        When clicar nas bolinhas de rolagem de anuncios
        And clicar nas setas de rolagem de anuncios
        Then valido que o conteudo apos rolagem

      @CT006
      Scenario: Validar Tela Test Cases
        Given acessar tela de cadastro
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar em Continue
        Given acessar tela Home
        When clicar no botao Test Case
        Then valido que a tela de Test Case foi apresentada

      @CT007
      Scenario: Validar Tela APIs List for Practice
        Given acessar tela de cadastro
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar em Continue
        Given acessar tela Home
        When clicar no botao APIs list for practice
        Then valido que a tela de APIs list for practice foi apresentada

      @CT008
      Scenario: Enviar E-mail para Subscrição
        Given acessar tela de cadastro
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar em Continue
        Given acessar tela Home
        And preencher email e clicar em enviar
        Then valido mensagem de subscricao feita com sucesso

      @CT009
      Scenario: Validar Rolagem da Pagina pela Seta no Canto Inferior Direito
        Given acessar tela de cadastro
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar em Continue
        Given acessar tela Home
        And descer para baixo e clicar na seta para voltar para o topo da pagina
        Then valido a permanencia na tela Home