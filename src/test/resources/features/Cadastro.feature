@Cadastro
  Feature: Tela de Cadastro

    Background: Realizar Testes na Tela de Cadastro
      Given acessar tela de cadastro

      @CT001
      Scenario: Cadastro de Conta Realizado
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        Then validar cadastro realizado com sucesso

      @CT002
      Scenario: Exclusão de Conta Realizada
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar em Continue
        And clicar em Delete
        Then validar conta deletada com sucesso

      @CT003
      Scenario: E-mail já Existente
        When preencher os campos iniciais
          | name  | email           |
          | Natan | natan@gmail.com |
        And clicar em Signup
        Then validar email ja existente