@Login
  Feature: Tela de Login

    Background: Realizar Testes na Tela de Login
      Given acessar tela de Login

      @CT001
      Scenario: Realizar Login com sucesso
        When preencher os campos iniciais para cadastro
        And clicar em Signup
        When preencher o resto dos campos para cadastro
        And clicar em Create Account
        And clicar na opcao Signup&Login
        And clicar em Logout
        When preencho os campos iniciais automatico
        And clicar em Login
        Then validar login realizado com sucesso

      @CT002
      Scenario: E-mail ou Senha incorretos
        When preencho os campos iniciais
          | email        | senha |
          | exemplo@gmail| 32132 |
        And clicar em Login
        Then validar email ou senha incorretos