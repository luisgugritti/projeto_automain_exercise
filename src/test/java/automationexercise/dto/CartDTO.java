package automationexercise.dto;

public class CartDTO {

    private String nameCard;
    private String numberCard;
    private String cvcCard;
    private String dtaMm;
    private String dtaYy;

    public String getNameCard() {
        return nameCard;
    }

    public void setNameCard(String nameCard) {
        this.nameCard = nameCard;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(String numberCard) {
        this.numberCard = numberCard;
    }

    public String getCvcCard() {
        return cvcCard;
    }

    public void setCvcCard(String cvcCard) {
        this.cvcCard = cvcCard;
    }

    public String getDtaMm() {
        return dtaMm;
    }

    public void setDtaMm(String dtaMm) {
        this.dtaMm = dtaMm;
    }

    public String getDtaYy() {
        return dtaYy;
    }

    public void setDtaYy(String dtaYy) {
        this.dtaYy = dtaYy;
    }

}
