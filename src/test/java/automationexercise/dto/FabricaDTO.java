package automationexercise.dto;

import io.cucumber.core.internal.com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;

public class FabricaDTO {

    public static CadastroDTO CadastroDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, CadastroDTO.class);
    }

    public static LoginDTO LoginDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, LoginDTO.class);
    }

    public static ContatoDTO ContatoDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, ContatoDTO.class);
    }

    public static CartDTO CartDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, CartDTO.class);
    }

    public static ProductsDTO ProductsDTO(Map<String, String> mapa) {
        return new ObjectMapper().convertValue(mapa, ProductsDTO.class);
    }

}
