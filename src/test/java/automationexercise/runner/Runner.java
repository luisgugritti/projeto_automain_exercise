package automationexercise.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        publish = false,
        stepNotifications = false,
        useFileNameCompatibleName = false,
        features = "src/test/resources/features",
        glue = "automationexercise",
        plugin = {
                "pretty",
                "junit:target/surefire-reports/result.xml",
                "html:target/cucumber-reports"
        },
        tags = "@Products and @CT008"
)
public class Runner {
}
