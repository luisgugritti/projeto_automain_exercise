package automationexercise.products.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class ProductsElements {

    @FindBy(xpath = "//input[@id='search_product']")
    public WebElement inputSearch;

    @FindBy(xpath = "//button[@id='submit_search']")
    public WebElement btnSearch;

    @FindBy(xpath = "//div[@class='productinfo text-center']")
    public WebElement typeProduct;

    @FindBy(xpath = "//span[@class='badge pull-right']")
    public List<WebElement> category;

    @FindBy(xpath = "//a[contains(text(), 'Dress')]")
    public List<WebElement> filtroDress;

    @FindBy(xpath = "//a[contains(text(), 'Tops')]")
    public List<WebElement> filtroTops;

    @FindBy(xpath = "//a[contains(text(), 'Saree')]")
    public List<WebElement> filtroSaree;

    @FindBy(xpath = "//a[contains(text(), 'Tshirts')]")
    public WebElement filtroTshirt;

    @FindBy(xpath = "//a[contains(text(), 'Jeans')]")
    public WebElement filtroJeans;

    @FindBy(xpath = "//h2[@class='title text-center']")
    public WebElement title;

    @FindBy(xpath = "//a[contains(text(), 'Tops & Shirts')]")
    public WebElement filtroTopsShirts;

    @FindBy(xpath = "//span[@class='pull-right']")
    public List<WebElement> marca;

}
