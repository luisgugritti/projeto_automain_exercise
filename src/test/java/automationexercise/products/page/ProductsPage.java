package automationexercise.products.page;

import automationexercise.base.BasePage;
import automationexercise.base.PageURLs;
import automationexercise.base.VariaveisGlobais;
import automationexercise.dto.ProductsDTO;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ProductsPage extends BasePage {

    WebDriver driver;
    ProductsElements productsElements;
    PageURLs urls;
    VariaveisGlobais varGlobal;

    public ProductsPage() {
        this.driver = BasePage.getDriver();
        productsElements = new ProductsElements();
        urls = new PageURLs();
        varGlobal = new VariaveisGlobais();
        PageFactory.initElements(driver, productsElements);
    }

    public void acessarTelaProdutos() {
        acessarUrl(urls.productsUrl);
    }

    public void preencherOCampoSearchProduct(ProductsDTO dto) {
        if (dto.getSearch() != null) {
            escrever(productsElements.inputSearch, dto.getSearch());
        }
    }

    public void validarProdutoPesquisadoNaCaixaEExibido() {
        String termoDeBusca = productsElements.inputSearch.getAttribute("value");
        esperarElemento(productsElements.typeProduct);

        Assert.assertTrue("Validando retorno de produto na tela após a pesquisa",
                productsElements.typeProduct.getText().contains(termoDeBusca));
    }

    public void selecionoCategoria(String categoria) {
        switch (categoria) {
            case "women":
                esperarElemento(productsElements.category.get(0));
                clicarBotao(productsElements.category.get(0));
                break;
            case "men":
                esperarElemento(productsElements.category.get(1));
                clicarBotao(productsElements.category.get(1));
                break;
            case "kids":
                esperarElemento(productsElements.category.get(2));
                clicarBotao(productsElements.category.get(2));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void selecionoFitlro(String filtro) {
        switch (filtro) {
            case "dress":
                esperarElemento(productsElements.filtroDress.get(0));
                clicarBotao(productsElements.filtroDress.get(0));
                break;
            case "tops":
                esperarElemento(productsElements.filtroTops.get(0));
                clicarBotao(productsElements.filtroTops.get(0));
                break;
            case "saree":
                esperarElemento(productsElements.filtroSaree.get(0));
                clicarBotao(productsElements.filtroSaree.get(0));
                break;
            case "tshirt":
                esperarElemento(productsElements.filtroTshirt);
                clicarBotao(productsElements.filtroTshirt);
                break;
            case "jeans":
                esperarElemento(productsElements.filtroJeans);
                clicarBotao(productsElements.filtroJeans);
                break;
            case "kids dress":
                esperarElemento(productsElements.filtroDress.get(1));
                clicarBotao(productsElements.filtroDress.get(1));
                break;
            case "tops shirts":
                esperarElemento(productsElements.filtroTopsShirts);
                clicarBotao(productsElements.filtroTopsShirts);
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void validoTelaFiltro(String telaFiltro) {
        switch (telaFiltro) {
            case "Women - Dress Products":
                Assert.assertTrue("Validando tela do filtro Dress",
                        productsElements.title.getText().trim().contains(varGlobal.titleDress));
                break;
            case "Women - Tops Products":
                Assert.assertTrue("Validando tela do filtro Tops",
                        productsElements.title.getText().trim().contains(varGlobal.titleTops));
                break;
            case "Women - Saree Products":
                Assert.assertTrue("Validando tela do filtro Saree",
                        productsElements.title.getText().trim().contains(varGlobal.titleSaree));
                break;
            case "Men - Tshirts Products":
                Assert.assertTrue("Validando tela do filtro Tshirt",
                        productsElements.title.getText().trim().contains(varGlobal.titleTshirt));
                break;
            case "Men - Jeans Products":
                Assert.assertTrue("Validando tela do filtro Jeans",
                        productsElements.title.getText().trim().contains(varGlobal.titleJeans));
                break;
            case "Kids - Dress Products":
                Assert.assertTrue("Validando tela do filtro Jeans",
                        productsElements.title.getText().trim().contains(varGlobal.titleKidsDress));
                break;
            case "Kids - Tops & Shirts Products":
                Assert.assertTrue("Validando tela do filtro Jeans",
                        productsElements.title.getText().trim().contains(varGlobal.titleTopsShirts));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void selecionoAMarca(String marca) {
        switch (marca) {
            case "polo":
                esperarElemento(productsElements.marca.get(0));
                clicarBotao(productsElements.marca.get(0));
                break;
            case "h&m":
                esperarElemento(productsElements.marca.get(1));
                clicarBotao(productsElements.marca.get(1));
                break;
            case "madame":
                esperarElemento(productsElements.marca.get(2));
                clicarBotao(productsElements.marca.get(2));
                break;
            case "mast&harbour":
                esperarElemento(productsElements.marca.get(3));
                clicarBotao(productsElements.marca.get(3));
                break;
            case "babyhug":
                esperarElemento(productsElements.marca.get(4));
                clicarBotao(productsElements.marca.get(4));
                break;
            case "allen solly junior":
                esperarElemento(productsElements.marca.get(5));
                clicarBotao(productsElements.marca.get(5));
                break;
            case "kookie kids":
                esperarElemento(productsElements.marca.get(6));
                clicarBotao(productsElements.marca.get(6));
                break;
            case "biba":
                esperarElemento(productsElements.marca.get(7));
                clicarBotao(productsElements.marca.get(7));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void validoATelaDaMarca(String telaMarca) {
        switch (telaMarca) {
            case "Brand - Polo Products":
                moverElemento(productsElements.title);

                Assert.assertTrue("Validando tela da marca Polo",
                        productsElements.title.getText().trim().contains(varGlobal.titlePolo));
                break;
            case "Brand - H&M Products":
                Assert.assertTrue("Validando tela da marca H&M",
                        productsElements.title.getText().trim().contains(varGlobal.titleHm));
                break;
            case "Brand - Madame Products":
                Assert.assertTrue("Validando tela da marca Madame",
                        productsElements.title.getText().trim().contains(varGlobal.titleMadame));
                break;
            case "Brand - Mast & Harbour Products":
                Assert.assertTrue("Validando tela da marca Mast",
                        productsElements.title.getText().trim().contains(varGlobal.titleMast));
                break;
            case "Brand - Babyhug Products":
                Assert.assertTrue("Validando tela da marca Babyhug",
                        productsElements.title.getText().trim().contains(varGlobal.titleBabyhug));
                break;
            case "Brand - Allen Solly Junior Products":
                Assert.assertTrue("Validando tela da marca Allen Solly",
                        productsElements.title.getText().trim().contains(varGlobal.titleAllen));
                break;
            case "Brand - Kookie Kids Products":
                Assert.assertTrue("Validando tela da marca Kookie",
                        productsElements.title.getText().trim().contains(varGlobal.titleKookie));
                break;
            case "Brand - Biba Products":
                Assert.assertTrue("Validando tela da marca Biba",
                        productsElements.title.getText().trim().contains(varGlobal.titleBiba));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

}