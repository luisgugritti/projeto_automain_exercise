package automationexercise.products.steps;

import automationexercise.dto.FabricaDTO;
import automationexercise.dto.ProductsDTO;
import automationexercise.products.page.ProductsPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;

public class ProductsSteps {

    ProductsPage productsPage;

    @Before
    public void before() {
        productsPage = new ProductsPage();
    }

    @Given("acessar tela Produtos")
    public void acessarTelaProdutos() {
        productsPage.acessarTelaProdutos();
    }

    @When("preencher o campo Search Product")
    public void preencherOCampoSearchProduct(List<Map<String, String>> mapa) {
        ProductsDTO dto = FabricaDTO.ProductsDTO(mapa.get(0));
        productsPage.preencherOCampoSearchProduct(dto);
    }

    @Then("valido que o produto pesquisado foi apresentado")
    public void validoQueOProdutoPesquisadoFoiApresentado() {
        productsPage.validarProdutoPesquisadoNaCaixaEExibido();
    }

    @When("^seleciono a categoria (.*)$")
    public void selecionoCategoria(String categoria) {
        productsPage.selecionoCategoria(categoria);
    }

    @And("^seleciono o filtro (.*)$")
    public void selecionoFitlro(String filtro) {
        productsPage.selecionoFitlro(filtro);
    }

    @Then("^valido a tela do filtro (.*)$")
    public void validoTelaFiltro(String telaFiltro) {
        productsPage.validoTelaFiltro(telaFiltro);
    }

    @When("^seleciono a marca (.*)$")
    public void selecionoAMarca(String marca) {
        productsPage.selecionoAMarca(marca);
    }

    @Then("^valido a tela da marca (.*)$")
    public void validoATelaDaMarca(String telaMarca) {
        productsPage.validoATelaDaMarca(telaMarca);
    }

}
