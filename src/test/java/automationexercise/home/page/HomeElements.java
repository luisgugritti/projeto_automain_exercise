package automationexercise.home.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomeElements {

    @FindBy(xpath = "//a[@class='cart_quantity_delete']")
    public WebElement btnExcluirItem;

    @FindBy(xpath = "//a[contains(text(), 'View Product')]")
    public List<WebElement> viewProduct;

    @FindBy(xpath = "//i[@class='fa fa-angle-right']")
    public List<WebElement> iconSetaRight;

    @FindBy(xpath = "//i[@class='fa fa-angle-left']")
    public List<WebElement> iconSetaLeft;

    @FindBy(xpath = "//li[@data-target='#slider-carousel']")
    public List<WebElement> iconRolagem;

    @FindBy(xpath = "//h2[contains(text(), 'Full-Fledged practice website for Automation Engineers')]")
    public List<WebElement> txtAnunCarousel;

    @FindBy(xpath = "//button[@class='btn btn-success']")
    public List<WebElement> btnTest;

    @FindBy(xpath = "//b[contains(text(), 'Test Cases')]")
    public WebElement txtTitleTestCase;

    @FindBy(xpath = "//b[contains(text(), 'APIs List for practice')]")
    public WebElement txtTitleAPIs;

    @FindBy(xpath = "//input[@id='susbscribe_email']")
    public WebElement inputCmpEmail;

    @FindBy(xpath = "//button[@id='subscribe']")
    public WebElement btnSubscribe;

    @FindBy(xpath = "//div[@class='alert-success alert']")
    public WebElement msgSubscribed;

    @FindBy(xpath = "//a[@id='scrollUp']")
    public WebElement iconSetaRolagem;

}
