package automationexercise.home.page;

import automationexercise.base.BasePage;
import automationexercise.base.PageURLs;
import automationexercise.base.VariaveisGlobais;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

    HomeElements homeElements;
    WebDriver driver;
    PageURLs urls;
    VariaveisGlobais varGlobal;

    public HomePage() {
        this.driver = BasePage.getDriver();
        homeElements = new HomeElements();
        urls = new PageURLs();
        varGlobal = new VariaveisGlobais();
        PageFactory.initElements(driver, homeElements);
    }

    public void acessarTelaHome() {
        acessarUrl(urls.homeUrl);
    }

    public void validoAPermanenciaNaTelaHome() {
        Assert.assertTrue("Validar a permanencia na tela Home após clicar em Continue Shopping",
                true);
    }

    public void validoQueATelaDeDetalhesDoProdutoFoiApresentada(String tela) {
        switch (tela) {
            case "detalhes do produto":
                Assert.assertTrue("Validar que o usuário foi movido para tela de detalhes do produto",
                        true);
                break;
            case "Test Case":
                Assert.assertTrue("Validar que o usuário foi movido para tela de Test Case",
                        homeElements.txtTitleTestCase.getText().contains(varGlobal.txtTitleTestCase));
                break;
            case "APIs list for practice":
                Assert.assertTrue("Validar que o usuário foi movido para tela de Test Case",
                        homeElements.txtTitleAPIs.getText().contains(varGlobal.txtTitleAPIs));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }

    }

    public void clicarNaDeRolagemDeAnuncios(String tipo) {
        switch (tipo) {
            case "bolinhas":
                esperarElemento(homeElements.iconRolagem.get(0));
                clicarBotao(homeElements.iconRolagem.get(0));
                clicarBotao(homeElements.iconRolagem.get(1));
                clicarBotao(homeElements.iconRolagem.get(2));
                break;
            case "setas":
                esperarElemento(homeElements.iconSetaRight.get(0));
                clicarBotao(homeElements.iconSetaRight.get(0));
                clicarBotao(homeElements.iconSetaLeft.get(0));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void validoQueOConteudoAposRolagem() {
        esperarElemento(homeElements.txtAnunCarousel.get(0));

        Assert.assertTrue("Validando conteúdo de anúncio no campo Carousel",
                homeElements.txtAnunCarousel.get(0).getText().trim().contains(varGlobal.txtCarousel));
    }

    public void preencherEmailClicarEnviar() {
        esperarElemento(homeElements.inputCmpEmail);
        moverElemento(homeElements.inputCmpEmail);
        escrever(homeElements.inputCmpEmail, "exemplo@gmail.com");
        clicarBotao(homeElements.btnSubscribe);
    }

    public void validoMensagemDeSubscricaoFeitaComSucesso() {
        esperarElemento(homeElements.msgSubscribed);

        Assert.assertTrue("Validando subscrição realizada com sucesso",
                homeElements.msgSubscribed.getText().trim().contains(varGlobal.txtSubscribe));
    }

    public void descerParaBaixoEClicarNaSetaParaVoltarParaOTopoDaPagina() {
        rolarParaBaixo();
        clicarBotao(homeElements.iconSetaRolagem);
    }

}
