package automationexercise.home.steps;

import automationexercise.home.page.HomePage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class HomeSteps {

    HomePage homePage;

    @Before
    public void before() {
        homePage = new HomePage();
    }

    @Given("acessar tela Home")
    public void acessarTelaHome() {
        homePage.acessarTelaHome();
    }

    @Then("valido a permanencia na tela Home")
    public void validoAPermanenciaNaTelaHome() {
        homePage.validoAPermanenciaNaTelaHome();
    }

    @Then("^valido que a tela de (.*) foi apresentada$")
    public void validoQueATelaDeDetalhesDoProdutoFoiApresentada(String tela) {
        homePage.validoQueATelaDeDetalhesDoProdutoFoiApresentada(tela);
    }

    @When("^clicar nas (.*) de rolagem de anuncios$")
    public void clicarNaDeRolagemDeAnuncios(String tipo) {
        homePage.clicarNaDeRolagemDeAnuncios(tipo);
    }

    @Then("valido que o conteudo apos rolagem")
    public void validoQueOConteudoAposRolagem() {
        homePage.validoQueOConteudoAposRolagem();
    }

    @And("preencher email e clicar em enviar")
    public void preencherEmailClicarEnviar() {
        homePage.preencherEmailClicarEnviar();
    }

    @Then("valido mensagem de subscricao feita com sucesso")
    public void validoMensagemDeSubscricaoFeitaComSucesso() {
        homePage.validoMensagemDeSubscricaoFeitaComSucesso();
    }

    @And("descer para baixo e clicar na seta para voltar para o topo da pagina")
    public void descerParaBaixoEClicarNaSetaParaVoltarParaOTopoDaPagina() {
        homePage.descerParaBaixoEClicarNaSetaParaVoltarParaOTopoDaPagina();
    }

}
