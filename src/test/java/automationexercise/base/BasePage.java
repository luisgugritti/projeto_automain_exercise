package automationexercise.base;

import automationexercise.dto.CadastroDTO;
import automationexercise.dto.CartDTO;
import automationexercise.dto.ContatoDTO;
import com.github.javafaker.Faker;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.time.YearMonth;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class BasePage {

    private static WebDriver driver;
    public static Faker faker = new Faker();
    private static CadastroDTO cadastroDTO;
    private static final Random random = new Random();
    private Actions actions;

    public static WebDriver getDriver() {
        if (driver == null) {
            startDriver();
        }
        return driver;
    }

    private static void startDriver() {

        try {
            ChromeOptions chromeOpts = new ChromeOptions();
            chromeOpts.addArguments("--lang=pt-br");
            chromeOpts.addArguments("start-maximized");
            chromeOpts.addArguments("--remote-allow-origins=*");
            chromeOpts.addArguments("ignore-certificate-errors");
            chromeOpts.addArguments("--no-sandbox");
            chromeOpts.addArguments("load-extension=C:/automacao/extensoes/uBlock");
            System.setProperty("webdriver.chrome.driver", "C:/automacao/webdriver/chromedriver.exe");
            driver = new ChromeDriver(chromeOpts);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Falha ao iniciar o ChromeDriver: " + e.getMessage());
        }

    }

    public static void acessarUrl(String url) {
        getDriver().get(url);
    }

    public void clicarBotao(WebElement btn) {
        if (btn != null) {
            btn.click();
        } else {
            throw new IllegalArgumentException("Botão não pode ser nulo");
        }
    }

    public void escrever(WebElement elemento, String write) {
        if (elemento != null && write != null) {
            elemento.sendKeys(write);
        } else {
            throw new IllegalArgumentException("Elemento e texto a ser escrito não podem ser nulos");
        }
    }

    public void escreverEntrar(WebElement elemento, String write) {
        if (elemento != null && write != null) {
            elemento.sendKeys(write);
            elemento.sendKeys(Keys.ENTER);
        } else {
            throw new IllegalArgumentException("Elemento e texto a ser escrito não podem ser nulos");
        }
    }

    public void limparCampo(WebElement clean) {
        if (clean != null) {
            clean.clear();
        } else {
            throw new IllegalArgumentException("Campo a ser limpo não pode ser nulo");
        }
    }

    public static void fechar() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    public void esperarUrl(String url) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(40));
        wait.until(ExpectedConditions.urlToBe(url));
    }

    public void esperarElemento(WebElement waitElement) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(40));
        wait.until(ExpectedConditions.visibilityOf(waitElement));
    }

    public void esperarElementos(List<WebElement> waitElements) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(40));
        wait.until(ExpectedConditions.visibilityOfAllElements(waitElements));
    }

    public void moverElemento(WebElement el) {
        JavascriptExecutor js = (JavascriptExecutor) this.driver;

        try {
            js.executeScript("arguments[0].scrollIntoView();", new Object[]{el});
        } catch (Exception var4) {
            System.out.println("Elemento não encontrado.");
        }
    }

    public String salvarConteudoCampo(WebElement campo) {
        if (campo != null) {
            return campo.getText();
        } else {
            throw new IllegalArgumentException("O campo não pode ser nulo");
        }
    }

    public void rolarParaTopo() {
        actions = new Actions(driver);
        actions.sendKeys(Keys.HOME).perform();
    }

    public void rolarParaBaixo() {
        actions = new Actions(driver);
        actions.sendKeys(Keys.END).perform();
    }

    public void selecionarPorClick(WebElement elemento) {
        if (elemento != null && elemento.isDisplayed() && elemento.isEnabled()) {
            boolean estaAberto = elemento.getAttribute("aria-expanded").equals("true");
            if (!estaAberto) {
                elemento.click();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void selecionarOpcaoPorValor(WebElement element, String valor) {
        Select select = new Select(element);
        select.selectByValue(valor);
    }

    public void selecionarOpcaoPorTexto(WebElement elemento, String opcao) {
        if (elemento != null && opcao != null) {
            Select select = new Select(elemento);
            select.selectByVisibleText(opcao);

            Actions actions = new Actions(driver);
            actions.moveToElement(elemento).click().build().perform();
        } else {
            throw new IllegalArgumentException("Elemento e opção a ser selecionada não podem ser nulos");
        }
    }

    public static void killChromeDriver() {
        ProcessBuilder processBuilder = new ProcessBuilder("taskkill", "/F", "/IM", "chromedriver.exe");

        try {
            Process process = processBuilder.start();
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CadastroDTO preencherCamposIniciaisAutomatico() {
        if (cadastroDTO == null) {
            cadastroDTO = new CadastroDTO();
            cadastroDTO.setName(faker.name().fullName());
            cadastroDTO.setEmail(faker.internet().emailAddress());
        }
        return cadastroDTO;
    }

    public CadastroDTO preencherRestoDosCamposAutomatico() {
        CadastroDTO dto = getCadastroDTO();

        if (dto == null) {
            throw new IllegalArgumentException("CadastroDTO não pode ser nulo");
        }

        if (dto.getSenha() == null) {
            dto.setTitulo(faker.name().title());
            dto.setSenha(faker.internet().password());
            gerarDataAutomatica(dto);
            dto.setFirstName(faker.name().firstName());
            dto.setLastName(faker.name().lastName());
            dto.setEmpresa(faker.company().name());
            dto.setEndereco(faker.address().streetAddress());
            dto.setEndereco2(faker.address().secondaryAddress());
            dto.setPais(faker.country().name());
            dto.setEstado(faker.address().state());
            dto.setCidade(faker.address().city());
            dto.setCep(faker.address().zipCode());
            dto.setTelefone(faker.phoneNumber().cellPhone());

            setCadastroDTO(dto);
        }
        return dto;
    }

    public static CadastroDTO getCadastroDTO()   {
        return cadastroDTO;
    }

    public static void setCadastroDTO(CadastroDTO cadastroDTO) {
        BasePage.cadastroDTO = cadastroDTO;
    }

    private void gerarDataAutomatica(CadastroDTO dto) {
        int year = faker.number().numberBetween(1900, 2100);
        int month = faker.number().numberBetween(1, 12);
        int maxDay = YearMonth.of(year, month).lengthOfMonth();
        int day = faker.number().numberBetween(1, maxDay + 1);

        dto.setAno(String.valueOf(year));
        dto.setMes(String.valueOf(month));
        dto.setDia(String.valueOf(day));
    }

    public ContatoDTO gerarDadosParaContato() {
        ContatoDTO dto = new ContatoDTO();

        dto.setNome(faker.name().fullName());
        dto.setEmail(faker.internet().emailAddress());
        dto.setAssunto(faker.book().title());
        dto.setMensagem(faker.lorem().characters());

        return dto;
    }

    public void fecharAlerta() {
        try {
            try {
                Alert alert = driver.switchTo().alert();
                System.out.println("Alerta encontrado com o texto: " + alert.getText());
                alert.accept();
            } catch (NoAlertPresentException e) {
                System.out.println("Nenhum alerta encontrado.");
            }
        } catch (UnhandledAlertException e) {
            try {
                Alert alert = driver.switchTo().alert();
                System.out.println("Alerta inesperado encontrado: " + alert.getText());
                alert.accept();
            } catch (NoAlertPresentException ex) {
                System.out.println("Nenhum alerta presente após a exceção.");
            }
        }
    }

    public CartDTO gerarDadosCredCard() {
        CartDTO dto = new CartDTO();

        dto.setNameCard(faker.name().fullName());
        dto.setNumberCard(faker.finance().creditCard());
        dto.setCvcCard(faker.number().digits(3));
        dto.setDtaMm(gerarMesDeValidade());
        dto.setDtaYy(gerarAnoDeValidade());

        return dto;
    }

    private static String gerarMesDeValidade() {
        int mes = random.nextInt(12) + 1;
        return String.format("%02d", mes);
    }

    private static String gerarAnoDeValidade() {
        int ano = random.nextInt(10) + 2024;
        return String.valueOf(ano);
    }

}