package automationexercise.base;

public class VariaveisGlobais {

    public String msgSucesso = "Congratulations! Your new account has been successfully created!";
    public String msgContaDeletada = "Your account has been permanently deleted!";
    public String msgEmailJaExiste = "Email Address already exist!";
    public String evidLoginSucesso = "Logged in as";
    public String msgEmailSenhaIncorretos = "Your email or password is incorrect!";
    public String msgEnviadaComSucesso = "Success! Your details have been submitted successfully.";
    public String itemAdcSucesso = "Added!";
    public String msgExclusaoItem = "Cart is empty!";
    public String qtdItem = "2";
    public String msgPagtoSucesso = "Congratulations! Your order has been confirmed!";
    public String txtCarousel = "Full-Fledged practice website for Automation Engineers";
    public String txtTitleTestCase = "TEST CASES";
    public String txtTitleAPIs = "APIS LIST FOR PRACTICE";
    public String txtSubscribe = "You have been successfully subscribed!";
    public String titleDress = "WOMEN - DRESS PRODUCTS";
    public String titleTops = "WOMEN - TOPS PRODUCTS";
    public String titleSaree = "WOMEN - SAREE PRODUCTS";
    public String titleTshirt = "MEN - TSHIRTS PRODUCTS";
    public String titleJeans = "MEN - JEANS PRODUCTS";
    public String titleKidsDress = "KIDS - DRESS PRODUCTS";
    public String titleTopsShirts = "KIDS - TOPS & SHIRTS PRODUCTS";
    public String titlePolo = "BRAND - POLO PRODUCTS";
    public String titleHm = "BRAND - H&M PRODUCTS";
    public String titleMadame = "BRAND - MADAME PRODUCTS";
    public String titleMast = "BRAND - MAST & HARBOUR PRODUCTS";
    public String titleBabyhug = "BRAND - BABYHUG PRODUCTS";
    public String titleAllen = "BRAND - ALLEN SOLLY JUNIOR PRODUCTS";
    public String titleKookie = "BRAND - KOOKIE KIDS PRODUCTS";
    public String titleBiba = "BRAND - BIBA PRODUCTS";


}
