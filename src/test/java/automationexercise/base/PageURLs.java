package automationexercise.base;

public class PageURLs {

    public String loginUrl = "https://www.automationexercise.com/login";
    public String cadastroUrl = "https://www.automationexercise.com/signup";
    public String productsUrl = "https://www.automationexercise.com/products";
    public String homeUrl = "https://www.automationexercise.com/";
    public String contactUrl = "https://www.automationexercise.com/contact_us";
    public String cartUrl = "https://www.automationexercise.com/view_cart";

}
