package automationexercise.cart.steps;

import automationexercise.cart.page.CartPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CartSteps {

    CartPage cartPage;

    @Before
    public void before() {
        cartPage = new CartPage();
    }

    @Given("acessar tela de carrinho de compras")
    public void acessatTelaCarrinhoCompras() {
        cartPage.acessatTelaCarrinhoCompras();
    }

    @And("^clicar no link (.*)$")
    public void clicarNoLink(String link) {
        cartPage.clicarNoLink(link);
    }

    @And("adiciona o produto no carrinho")
    public void adcProdutoCarrinho() {
        cartPage.adcProdutoCarrinho();
    }

    @And("clicar no icone de exclusao")
    public void clicarIconeExclusao() {
        cartPage.clicarIconeExclusao();
    }

    @And("^clicar no botao (.*)$")
    public void clicarBotaoCart(String btn) {
        cartPage.clicarBotaoCart(btn);
    }

    @And("escrever comentario sobre processo de entrega")
    public void escreverComntEntrega() {
        cartPage.escreverComntEntrega();
    }

    @Then("^valido que item foi (.*)$")
    public void validarItemFoi(String evento) {
        cartPage.validarItemAdcCarrinho(evento);
    }

    @When("preenche os campos com os dados do cartao de credito")
    public void preencheOsCamposComOsDadosDoCartaoDeCredito() {
        cartPage.preencheOsCamposComOsDadosDoCartaoDeCredito();
    }

}
