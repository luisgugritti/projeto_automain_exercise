package automationexercise.cart.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class CartElements {

    @FindBy(xpath = "//u[contains(text(), 'here')]")
    public WebElement linkClickHere;

    @FindBy(xpath = "//a[contains(text(), 'Cart')]")
    public WebElement menuCart;

    @FindBy(xpath = "//a[@class='btn btn-default add-to-cart' and @data-product-id='1']")
    public List<WebElement> btnAddCart;

    @FindBy(xpath = "//u[contains(text(), 'View Cart')]")
    public WebElement viewCart;

    @FindBy(xpath = "//h4[@class='modal-title w-100']")
    public WebElement msgItemAddComSucesso;

    @FindBy(xpath = "//a[@class='cart_quantity_delete']")
    public WebElement btnExcluirItem;

    @FindBy(xpath = "//b[contains(text(), 'Cart is empty!')]")
    public WebElement msgCartEmpty;

    @FindBy(xpath = "//button[contains(text(), '2')]")
    public WebElement adc2Product;

    @FindBy(xpath = "//button[@class='btn btn-success close-modal btn-block']")
    public WebElement btnKeepBuy;

    @FindBy(xpath = "//a[@class='btn btn-default check_out']")
    public WebElement btnCheckout;

    @FindBy(xpath = "//textarea[@name='message']")
    public WebElement cmpTextComent;

    @FindBy(xpath = "//a[@class='btn btn-default check_out']")
    public WebElement btnPlaceOrder;

    @FindBy(xpath = "//input[@name='name_on_card']")
    public WebElement inputNameCard;

    @FindBy(xpath = "//input[@name='card_number']")
    public WebElement inputNumberCard;

    @FindBy(xpath = "//input[@name='cvc']")
    public WebElement inputCvc;

    @FindBy(xpath = "//input[@name='expiry_month']")
    public WebElement inputMm;

    @FindBy(xpath = "//input[@name='expiry_year']")
    public WebElement inputYyyy;

    @FindBy(xpath = "//button[@id='submit']")
    public WebElement btnConfrmOrder;

    @FindBy(xpath = "//p[@style='font-size: 20px; font-family: garamond;']")
    public WebElement msgPgtoSucesso;

}
