package automationexercise.cart.page;

import automationexercise.base.BasePage;
import automationexercise.base.PageURLs;
import automationexercise.base.VariaveisGlobais;
import automationexercise.dto.CartDTO;
import automationexercise.home.page.HomeElements;
import automationexercise.products.page.ProductsElements;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CartPage extends BasePage {

    CartElements cartElements;
    PageURLs urls;
    WebDriver driver;
    VariaveisGlobais varGlobal;
    HomeElements homeElements;
    ProductsElements productsElements;

    public CartPage() {
        this.driver = BasePage.getDriver();
        cartElements = new CartElements();
        urls = new PageURLs();
        varGlobal = new VariaveisGlobais();
        homeElements = new HomeElements();
        productsElements = new ProductsElements();
        PageFactory.initElements(driver, cartElements);
        PageFactory.initElements(driver, homeElements);
        PageFactory.initElements(driver, productsElements);
    }

    public void acessatTelaCarrinhoCompras() {
        acessarUrl(urls.cartUrl);
    }

    public void clicarNoLink(String link) {
        switch (link) {
            case "Here":
                esperarElemento(cartElements.linkClickHere);
                clicarBotao(cartElements.linkClickHere);
                break;
            case "View Cart":
                esperarElemento(cartElements.viewCart);
                clicarBotao(cartElements.viewCart);
                break;
            case "View Product":
                esperarElemento(homeElements.viewProduct.get(0));
                clicarBotao(homeElements.viewProduct.get(0));
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void adcProdutoCarrinho() {
        esperarElemento(cartElements.btnAddCart.get(0));
        moverElemento(cartElements.btnAddCart.get(0));
        clicarBotao(cartElements.btnAddCart.get(0));
    }

    public void validarItemAdcCarrinho(String evento) {
        switch (evento) {
            case "adicionado ao carrinho":
                esperarElemento(cartElements.msgItemAddComSucesso);

                Assert.assertTrue("Validando adição do item ao carrinho de compras",
                        cartElements.msgItemAddComSucesso.getText().trim().contains(varGlobal.itemAdcSucesso));
                break;
            case "excluido do carrinho":
                esperarElemento(cartElements.msgCartEmpty);

                Assert.assertTrue("Validando adição do item ao carrinho de compras",
                        cartElements.msgCartEmpty.getText().trim().contains(varGlobal.msgExclusaoItem));
                break;
            case "adicionado duas vezes":
                esperarElemento(cartElements.adc2Product);

                Assert.assertTrue("Validando adição de mais de um item ao carrinho de compras",
                        cartElements.adc2Product.getText().trim().contains(varGlobal.qtdItem));
                break;
            case "pago com sucesso":
                esperarElemento(cartElements.msgPgtoSucesso);

                Assert.assertTrue("Validando pagamento realizado com sucesso",
                        cartElements.msgPgtoSucesso.getText().trim().contains(varGlobal.msgPagtoSucesso));
                break;
            case "adicionado ao carrinho com icone de exclusao":
                esperarElemento(homeElements.btnExcluirItem);

                Assert.assertTrue("Validando inclusão de item no carrinho junto ao botão de exlusão",
                        homeElements.btnExcluirItem.isDisplayed());
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void clicarIconeExclusao() {
        esperarElemento(cartElements.btnExcluirItem);
        clicarBotao(cartElements.btnExcluirItem);
    }


    public void clicarBotaoCart(String btn) {
        switch (btn) {
            case "Continue Shopping":
                esperarElemento(cartElements.btnKeepBuy);
                clicarBotao(cartElements.btnKeepBuy);
                break;
            case "Proceed To Checkout":
                esperarElemento(cartElements.btnCheckout);
                clicarBotao(cartElements.btnCheckout);
                break;
            case "Place Order":
                esperarElemento(cartElements.btnPlaceOrder);
                clicarBotao(cartElements.btnPlaceOrder);
                break;
            case "Pay and Confirm Order":
                esperarElemento(cartElements.btnConfrmOrder);
                clicarBotao(cartElements.btnConfrmOrder);
                break;
            case "Test Case":
                esperarElemento(homeElements.btnTest.get(0));
                clicarBotao(homeElements.btnTest.get(0));
                break;
            case "APIs list for practice":
                esperarElemento(homeElements.btnTest.get(1));
                clicarBotao(homeElements.btnTest.get(1));
                break;
            case "Search Product":
                esperarElemento(productsElements.btnSearch);
                clicarBotao(productsElements.btnSearch);
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

    public void escreverComntEntrega() {
        esperarElemento(cartElements.cmpTextComent);
        escrever(cartElements.cmpTextComent, "Teste Automatizado");
    }

    public void preencheOsCamposComOsDadosDoCartaoDeCredito() {
        CartDTO dto = gerarDadosCredCard();

        if (dto.getNameCard() != null) {
            esperarElemento(cartElements.inputNameCard);
            escrever(cartElements.inputNameCard, dto.getNameCard());
        }
        if (dto.getNumberCard() != null) {
            esperarElemento(cartElements.inputNumberCard);
            escrever(cartElements.inputNumberCard, dto.getNumberCard());
        }
        if (dto.getCvcCard() != null) {
            esperarElemento(cartElements.inputCvc);
            escrever(cartElements.inputCvc, dto.getCvcCard());
        }
        if (dto.getDtaMm() != null) {
            esperarElemento(cartElements.inputMm);
            escrever(cartElements.inputMm, dto.getDtaMm());
        }
        if (dto.getDtaYy() != null) {
            esperarElemento(cartElements.inputYyyy);
            escrever(cartElements.inputYyyy, dto.getDtaYy());
        }
    }

}
