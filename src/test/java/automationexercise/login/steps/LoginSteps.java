package automationexercise.login.steps;

import automationexercise.cadastro.page.CadastroPage;
import automationexercise.dto.FabricaDTO;
import automationexercise.dto.LoginDTO;
import automationexercise.login.page.LoginPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;

public class LoginSteps {

    LoginPage loginPage;

    @Before
    public void before() {
        loginPage = new LoginPage();
    }

    @Given("acessar tela de Login")
    public void acessarTelaDeLogin() {
        loginPage.acessarTelaDeLogin();
    }

    @When("preencho os campos iniciais")
    public void preenchoOsCamposIniciais(List<Map<String, String>> mapa) {
        LoginDTO dto = FabricaDTO.LoginDTO(mapa.get(0));
        loginPage.preenchoOsCamposIniciais(dto);
    }

    @When("preencho os campos iniciais automatico")
    public void preenchoOsCamposIniciaisAutomatico() {
        CadastroPage cadastroPage = new CadastroPage();
        cadastroPage.preencherCamposIniciaisAutomatico();
        cadastroPage.preencherRestoDosCamposAutomatico();
        LoginPage loginPage = new LoginPage();
        loginPage.preenchoOsCamposIniciaisAutomatico();
    }

    @And("clicar na opcao Signup&Login")
    public void clicarEmSignupLogin() {
        loginPage.clicarEmSignupLogin();
    }

}
