package automationexercise.login.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginElements {

    @FindBy(xpath = "//input[@name='email' and @data-qa='login-email']")
    public WebElement email;

    @FindBy(xpath = "//input[@name='password' and @data-qa='login-password']")
    public WebElement senha;

    @FindBy(xpath = "//a[contains(text(), ' Signup / Login')]")
    public WebElement signupLogin;

    @FindBy(xpath = "//button[@class='btn btn-default' and @data-qa='login-button']")
    public WebElement btnLogin;

    @FindBy(xpath = "//a[contains(text(), 'Logged in as')]")
    public WebElement evidLoginSucesso;

    @FindBy(xpath = "//a[contains(text(), 'Logout')]")
    public WebElement logout;

    @FindBy(xpath = "//p[contains(text(), 'Your email or password is incorrect!')]")
    public WebElement msgEmailSenhaIncorretos;

}
