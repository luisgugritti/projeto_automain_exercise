package automationexercise.login.page;

import automationexercise.base.BasePage;
import automationexercise.base.PageURLs;
import automationexercise.base.VariaveisGlobais;
import automationexercise.dto.CadastroDTO;
import automationexercise.dto.LoginDTO;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    LoginElements elements;
    WebDriver driver;
    VariaveisGlobais globalVar;
    PageURLs urls;

    public LoginPage() {
        this.driver = BasePage.getDriver();
        elements = new LoginElements();
        globalVar = new VariaveisGlobais();
        urls = new PageURLs();
        PageFactory.initElements(driver, elements);
    }

    public void acessarTelaDeLogin() {
        acessarUrl(urls.loginUrl);
    }

    public void preenchoOsCamposIniciais(LoginDTO dto) {
        if (dto.getEmail() != null) {
            esperarElemento(elements.email);
            escrever(elements.email, dto.getEmail());
        }

        if (dto.getSenha() != null) {
            esperarElemento(elements.senha);
            escrever(elements.senha, dto.getSenha());
        }
    }

    public void preencherCampoEmail(String email) {
        esperarElemento(elements.email);
        escrever(elements.email, email);
    }

    public void preencherCampoSenha(String senha) {
        esperarElemento(elements.senha);
        escrever(elements.senha, senha);
    }

    public void preenchoOsCamposIniciaisAutomatico() {
        CadastroDTO cadastroDTO = BasePage.getCadastroDTO();
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setEmail(cadastroDTO.getEmail());
        loginDTO.setSenha(cadastroDTO.getSenha());

        preencherCampoEmail(loginDTO.getEmail());
        preencherCampoSenha(loginDTO.getSenha());
    }

    public void clicarEmSignupLogin() {
        esperarElemento(elements.signupLogin);
        clicarBotao(elements.signupLogin);
    }

}
