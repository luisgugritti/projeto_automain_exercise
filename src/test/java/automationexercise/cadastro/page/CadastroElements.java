package automationexercise.cadastro.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CadastroElements {

    @FindBy(xpath = "//input[@name='name']")
    public WebElement inputTxtName;

    @FindBy(xpath = "//input[@name='email' and @data-qa='signup-email']")
    public WebElement inputTxtEmail;

    @FindBy(xpath = "//input[@id='password']")
    public WebElement inputTxtSenha;

    @FindBy(xpath = "//button[@class='btn btn-default' and @data-qa='signup-button']")
    public WebElement btnSignup;

    @FindBy(xpath = "//p[contains(text(), 'Congratulations! Your new account has been successfully created!')]")
    public WebElement msgCadastroSucesso;

    @FindBy(xpath = "//input[@id='id_gender1']")
    public WebElement inputCmpTitle;

    @FindBy(xpath = "//select[@id='days']")
    public WebElement selectDay;

    @FindBy(xpath = "//select[@id='months']")
    public WebElement selectMonth;

    @FindBy(xpath = "//select[@id='years']")
    public WebElement selectYear;

    @FindBy(xpath = "//input[@id='first_name']")
    public WebElement inputCmpFirstName;

    @FindBy(xpath = "//input[@id='last_name']")
    public WebElement inputCmpLastName;

    @FindBy(xpath = "//input[@id='company']")
    public WebElement inputCmpCompany;

    @FindBy(xpath = "//input[@id='address1']")
    public WebElement inputCmpAddress;

    @FindBy(xpath = "//input[@id='address2']")
    public WebElement inputCmpAddress2;

    @FindBy(xpath = "//select[@id='country']")
    public WebElement selectCountry;

    @FindBy(xpath = "//input[@id='state']")
    public WebElement inputCmpState;

    @FindBy(xpath = "//input[@id='city']")
    public WebElement inputCmpCity;

    @FindBy(xpath = "//input[@id='zipcode']")
    public WebElement inputCmpZipCode;

    @FindBy(xpath = "//input[@id='mobile_number']")
    public WebElement inputCmpPhoneNumber;

    @FindBy(xpath = "//button[@class='btn btn-default' and @data-qa='create-account']")
    public WebElement btnCreateAccount;

    @FindBy(xpath = "//a[@class='btn btn-primary']")
    public WebElement btnContinue;

    @FindBy(xpath = "//a[contains(text(), 'Delete Account')]")
    public WebElement linkDeletedAccount;

    @FindBy(xpath = "//p[contains(text(), 'Your account has been permanently deleted!')]")
    public WebElement msgContaDeletada;

    @FindBy(xpath = "//p[@style='color: red;']")
    public WebElement msgEmailJaExiste;

    @FindBy(xpath = "//input[@id='newsletter']")
    public WebElement inputSignupOurNewsLetter;

    @FindBy(xpath = "//input[@id='optin']")
    public WebElement inputReceiveSpecial;

}
