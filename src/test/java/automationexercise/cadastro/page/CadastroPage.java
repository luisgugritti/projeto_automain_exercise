package automationexercise.cadastro.page;

import automationexercise.base.BasePage;
import automationexercise.base.PageURLs;
import automationexercise.base.VariaveisGlobais;
import automationexercise.cart.page.CartElements;
import automationexercise.contact.page.ContactUsElements;
import automationexercise.dto.CadastroDTO;
import automationexercise.login.page.LoginElements;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CadastroPage extends BasePage {

    CadastroElements cadastroElements;
    WebDriver driver;
    PageURLs urls;
    VariaveisGlobais globalVar;
    LoginElements elementsLogin;
    ContactUsElements elementsContact;
    CartElements cartElements;

    public CadastroPage() {
        this.driver = BasePage.getDriver();
        cadastroElements = new CadastroElements();
        urls = new PageURLs();
        globalVar = new VariaveisGlobais();
        elementsLogin = new LoginElements();
        elementsContact = new ContactUsElements();
        cartElements = new CartElements();
        PageFactory.initElements(driver, cadastroElements);
        PageFactory.initElements(driver, elementsLogin);
        PageFactory.initElements(driver, elementsContact);
        PageFactory.initElements(driver, cartElements);
    }

    public void acessarTelaCadastro() {
        acessarUrl(urls.cadastroUrl);
    }

    public void preencherCampos(CadastroDTO dto) {
        if (dto.getName() != null) {
            esperarElemento(cadastroElements.inputTxtName);
            escrever(cadastroElements.inputTxtName, dto.getName());
        }
        if (dto.getEmail() != null) {
            esperarElemento(cadastroElements.inputTxtEmail);
            escrever(cadastroElements.inputTxtEmail, dto.getEmail());
        }
    }

    public void preencherCamposIniciais() {
        CadastroDTO dto = preencherCamposIniciaisAutomatico();

        if (dto.getName() != null) {
            esperarElemento(cadastroElements.inputTxtName);
            escrever(cadastroElements.inputTxtName, dto.getName());
        }
        if (dto.getEmail() != null) {
            esperarElemento(cadastroElements.inputTxtEmail);
            escrever(cadastroElements.inputTxtEmail, dto.getEmail());
        }
    }

    public void preencherRestoDosCampos() {
        CadastroDTO dto = preencherRestoDosCamposAutomatico();

        if (dto.getTitulo() != null) {
            moverElemento(cadastroElements.inputCmpTitle);
            esperarElemento(cadastroElements.inputCmpTitle);
            escrever(cadastroElements.inputCmpTitle, dto.getTitulo());
        }
        if (dto.getSenha() != null) {
            esperarElemento(cadastroElements.inputTxtSenha);
            moverElemento(cadastroElements.inputTxtSenha);
            escrever(cadastroElements.inputTxtSenha, dto.getSenha());
        }
        if (dto.getDia() != null) {
            esperarElemento(cadastroElements.selectDay);
            moverElemento(cadastroElements.selectDay);
            escrever(cadastroElements.selectDay, dto.getDia());
        }
        if (dto.getMes() != null) {
            esperarElemento(cadastroElements.selectMonth);
            moverElemento(cadastroElements.selectMonth);
            escrever(cadastroElements.selectMonth, dto.getMes());
        }
        if (dto.getAno() != null) {
            esperarElemento(cadastroElements.selectYear);
            moverElemento(cadastroElements.selectYear);
            escrever(cadastroElements.selectYear, dto.getAno());
        }
        if (dto.getFirstName() != null) {
            esperarElemento(cadastroElements.inputCmpFirstName);
            moverElemento(cadastroElements.inputCmpFirstName);
            escrever(cadastroElements.inputCmpFirstName, dto.getFirstName());
        }
        if (dto.getLastName() != null) {
            esperarElemento(cadastroElements.inputCmpLastName);
            moverElemento(cadastroElements.inputCmpLastName);
            escrever(cadastroElements.inputCmpLastName, dto.getLastName());
        }
        if (dto.getEmpresa() != null) {
            esperarElemento(cadastroElements.inputCmpCompany);
            moverElemento(cadastroElements.inputCmpCompany);
            escrever(cadastroElements.inputCmpCompany, dto.getEmpresa());
        }
        if (dto.getEndereco() != null) {
            esperarElemento(cadastroElements.inputCmpAddress);
            moverElemento(cadastroElements.inputCmpAddress);
            escrever(cadastroElements.inputCmpAddress, dto.getEndereco());
        }
        if (dto.getEndereco2() != null) {
            esperarElemento(cadastroElements.inputCmpAddress2);
            moverElemento(cadastroElements.inputCmpAddress2);
            escrever(cadastroElements.inputCmpAddress2, dto.getEndereco2());
        }
        if (dto.getPais() != null) {
            esperarElemento(cadastroElements.selectCountry);
            moverElemento(cadastroElements.selectCountry);
            escrever(cadastroElements.selectCountry, dto.getPais());
        }
        if (dto.getEstado() != null) {
            esperarElemento(cadastroElements.inputCmpState);
            moverElemento(cadastroElements.inputCmpState);
            escrever(cadastroElements.inputCmpState, dto.getEstado());
        }
        if (dto.getCidade() != null) {
            esperarElemento(cadastroElements.inputCmpCity);
            moverElemento(cadastroElements.inputCmpCity);
            escrever(cadastroElements.inputCmpCity, dto.getCidade());
        }
        if (dto.getCep() != null) {
            esperarElemento(cadastroElements.inputCmpZipCode);
            moverElemento(cadastroElements.inputCmpZipCode);
            escrever(cadastroElements.inputCmpZipCode, dto.getCep());
        }
        if (dto.getTelefone() != null) {
            esperarElemento(cadastroElements.inputCmpPhoneNumber);
            moverElemento(cadastroElements.inputCmpPhoneNumber);
            escrever(cadastroElements.inputCmpPhoneNumber, dto.getTelefone());
        }
    }

    public void validarCenario(String cenario) {
        if (cenario.equals("cadastro realizado com sucesso")) {
            esperarElemento(cadastroElements.msgCadastroSucesso);

            Assert.assertTrue("Validando cadastro realizado com sucesso",
                    cadastroElements.msgCadastroSucesso.getText().trim().contains(globalVar.msgSucesso));
        }
        if (cenario.equals("conta deletada com sucesso")) {
            esperarElemento(cadastroElements.msgContaDeletada);

            Assert.assertTrue("Validando mensagem sobre conta deletada com sucesso",
                    cadastroElements.msgContaDeletada.getText().trim().contains(globalVar.msgContaDeletada));
        }
        if (cenario.equals("email ja existente")) {
            esperarElemento(cadastroElements.msgEmailJaExiste);

            Assert.assertTrue("Validando mensagem sobre e-mail ja existente",
                    cadastroElements.msgEmailJaExiste.getText().trim().contains(globalVar.msgEmailJaExiste));
        }
        if (cenario.equals("login realizado com sucesso")) {
            esperarElemento(elementsLogin.evidLoginSucesso);

            Assert.assertTrue("Validando mensagem sobre e-mail ja existente",
                    elementsLogin.evidLoginSucesso.getText().trim().contains(globalVar.evidLoginSucesso));
        }
        if (cenario.equals("email ou senha incorretos")) {
            esperarElemento(elementsLogin.msgEmailSenhaIncorretos);

            Assert.assertTrue("Validando mensagem sobre e-mail ou senha incorretos",
                    elementsLogin.msgEmailSenhaIncorretos.getText().trim().contains(globalVar.msgEmailSenhaIncorretos));
        }
        if (cenario.equals("mensagem enviada com sucesso")) {
            esperarElemento(elementsContact.msgEnviadaComSucesso);
            moverElemento(elementsContact.msgEnviadaComSucesso);

            Assert.assertTrue("Validando mensagem enviada com sucesso",
                    elementsContact.msgEnviadaComSucesso.getText().trim().contains(globalVar.msgEnviadaComSucesso));
        }
    }

    public void selecionarAssinaturaReceberOfertas() {
        esperarElemento(cadastroElements.inputSignupOurNewsLetter);
        selecionarPorClick(cadastroElements.inputSignupOurNewsLetter);
        esperarElemento(cadastroElements.inputReceiveSpecial);
        selecionarPorClick(cadastroElements.inputReceiveSpecial);
    }

    public void clicarEm(String tipoBtn) {
        switch (tipoBtn) {
            case "Signup":
                esperarElemento(cadastroElements.btnSignup);
                clicarBotao(cadastroElements.btnSignup);
                break;
            case "Create Account":
                esperarElemento(cadastroElements.btnCreateAccount);
                clicarBotao(cadastroElements.btnCreateAccount);
                break;
            case "Continue":
                esperarElemento(cadastroElements.btnContinue);
                clicarBotao(cadastroElements.btnContinue);
                break;
            case "Delete":
                esperarElemento(cadastroElements.linkDeletedAccount);
                clicarBotao(cadastroElements.linkDeletedAccount);
                break;
            case "Login":
                esperarElemento(elementsLogin.btnLogin);
                clicarBotao(elementsLogin.btnLogin);
                break;
            case "Logout":
                esperarElemento(elementsLogin.logout);
                clicarBotao(elementsLogin.logout);
                break;
            case "ContactUs":
                esperarElemento(elementsContact.menuContactus);
                clicarBotao(elementsContact.menuContactus);
                break;
            case "Submit":
                esperarElemento(elementsContact.btnSubmit);
                moverElemento(elementsContact.btnSubmit);
                clicarBotao(elementsContact.btnSubmit);
                fecharAlerta();
                break;
            case "Cart":
                esperarElemento(cartElements.menuCart);
                clicarBotao(cartElements.menuCart);
                break;
            default:
                throw new IllegalArgumentException("Argumento inválido!");
        }
    }

}
