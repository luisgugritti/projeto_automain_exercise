package automationexercise.cadastro.steps;

import automationexercise.cadastro.page.CadastroPage;
import automationexercise.dto.CadastroDTO;
import automationexercise.dto.FabricaDTO;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;

public class CadastroSteps {

    CadastroPage cadastroPage;
    @Before
    public void config() {
        cadastroPage = new CadastroPage();
    }

    @Given("acessar tela de cadastro")
    public void acessarTelaCadastro() {
        cadastroPage.acessarTelaCadastro();
    }

    @When("^preencher os campos iniciais$")
    public void preencherCampos(List<Map<String, String>> mapa) {
        CadastroDTO dto = FabricaDTO.CadastroDTO(mapa.get(0));
        cadastroPage.preencherCampos(dto);
    }

    @When("preencher os campos iniciais para cadastro")
    public void preencherCamposIniciais() {
        cadastroPage.preencherCamposIniciais();
    }

    @When("preencher o resto dos campos para cadastro")
    public void preencherRestoDosCampos() {
        cadastroPage.preencherRestoDosCampos();
    }

    @And("^clicar em (.*)$")
    public void clicarEm(String tipoBtn) {
        cadastroPage.clicarEm(tipoBtn);
    }

    @And("^seleciona a assinatura da newsletter e deseja receber ofertas especiais$")
    public void selecionarAssinaturaReceberOfertas() {
        cadastroPage.selecionarAssinaturaReceberOfertas();
    }

    @Then("^validar (.*)$")
    public void validarCenario(String cenario) {
        cadastroPage.validarCenario(cenario);
    }

}
