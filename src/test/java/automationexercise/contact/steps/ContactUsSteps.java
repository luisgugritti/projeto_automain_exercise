package automationexercise.contact.steps;

import automationexercise.contact.page.ContactUsPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class ContactUsSteps {

    ContactUsPage contatoPage;

    @Before
    public void before() {
        contatoPage = new ContactUsPage();
    }

    @Given("acessar tela de Contato")
    public void acessarTelaDContato() {
        contatoPage.acessarTelaDContato();
    }

    @When("preencher os campos de contato")
    public void preencherCamposContato() {
        contatoPage.preencherCamposContato();
    }

}
