package automationexercise.contact.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ContactUsElements {

    @FindBy(xpath = "//a[contains(text(), ' Contact us')]")
    public WebElement menuContactus;

    @FindBy(xpath = "//input[@name='name']")
    public WebElement nome;

    @FindBy(xpath = "//input[@name='email']")
    public WebElement email;

    @FindBy(xpath = "//input[@name='subject']")
    public WebElement subject;

    @FindBy(xpath = "//textarea[@id='message']")
    public WebElement message;

    @FindBy(xpath = "//input[@name='submit' and @class='btn btn-primary pull-left submit_form']")
    public WebElement btnSubmit;

    @FindBy(xpath = "//div[@class='status alert alert-success']")
    public WebElement msgEnviadaComSucesso;

}
