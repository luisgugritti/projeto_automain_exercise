package automationexercise.contact.page;

import automationexercise.base.BasePage;
import automationexercise.base.PageURLs;
import automationexercise.dto.ContatoDTO;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ContactUsPage extends BasePage {

    ContactUsElements contatoElements;
    WebDriver driver;
    PageURLs urls;

    public ContactUsPage() {
        this.driver = BasePage.getDriver();
        contatoElements = new ContactUsElements();
        urls = new PageURLs();
        PageFactory.initElements(driver, contatoElements);
    }

    public void acessarTelaDContato() {
        acessarUrl(urls.contactUrl);
    }

    public void preencherCamposContato() {
        ContatoDTO dto = gerarDadosParaContato();

        if (dto.getNome() != null) {
            esperarElemento(contatoElements.nome);
            escrever(contatoElements.nome, dto.getNome());
        }
        if (dto.getEmail() != null) {
            esperarElemento(contatoElements.email);
            escrever(contatoElements.email, dto.getEmail());
        }
        if (dto.getAssunto() != null) {
            esperarElemento(contatoElements.subject);
            escrever(contatoElements.subject, dto.getAssunto());
        }
        if (dto.getMensagem() != null) {
            esperarElemento(contatoElements.message);
            escrever(contatoElements.message, dto.getMensagem());
        }
    }

}
